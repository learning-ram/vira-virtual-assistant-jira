'use strict';
/* 
NOTE:
KURANG RESET STATE KETIKA USER SUDAH KONFIRM YA / TIDAK
ERROR HANDLING KETIKA USER INTERRUPT DITENGAH-TENGAH STATE
*/
const express = require('express');
const bodyParser = require('body-parser');
let send = require('./lib/sendMsg')
const axios = require('axios');
if (process.env.NODE_ENV === 'production') {
    require('custom-env').env('production')
    console.log("You're in Production")
    console.log("You will hit endpoint from: " + process.env.apiUrl)
} else {
    require('custom-env').env('development')
    console.log("You're in Development")
    console.log("You will hit endpoint from: " + process.env.apiUrl)
}

const server = express();
server.use(bodyParser.urlencoded({
    extended: true
}));
const list = require('./list_bot.json')

server.use(bodyParser.json());

//variabel default
let nama
let pesan = ''
let room_id
let payload
let state = {}
let reqData = {}
var team = ''

var JiraClient = require("jira-connector");

var jira = new JiraClient({
    host: "telkomdds.atlassian.net",
    basic_auth: {
        email: "chataja.ramaadtym@gmail.com",
        api_token: `${process.env.apiToken}`
    }
});

function closed(data) {
    let len = data.values.length - 1
    console.log(data.values[len - 1].state)
    if (data.values[len - 1].state === 'closed') {
        send.Txt('Maaf, sprint ' + data.values[len - 1].name + ' sudah ditutup. Tunggu sprint selanjutnya ya', room_id)
    }
}

function filterTask(data) {
    data.values.filter((x) => {
        if (x.state === 'active') {
            jira.sprint.getSprintIssues({
                sprintId: x.id
            }, (err, data) => {
                data.issues.filter((y) => {
                    if (y.fields.status.name === 'TO DO') {
                        var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                        comment = comment + "\n" +
                            y.key + "\n" +
                            "Backlog: " + y.fields.summary + "\n" +
                            "PIC: " + pic + "\n" +
                            "Status: " + y.fields.status.name + "\n"
                    }
                })
                comment = "🚨 Backlog To Do di " + x.name + " 🚨\n\n" + comment
                send.Txt(comment, room_id)
            })
        }
    })
    closed(data)
}

function sendtoPO(){
    axios.post(`${process.env.apiSearchContact}`,{
        access_token:`${process.env.access_token_adm}`,
        query:"+6285711424734" //nomor PO
    }).then((res)=>{
        axios.post(`${process.env.apiAdminContact}`,{
            access_token:`${process.env.access_token_adm}`,
            user_id:res.data.data[0].id,
            contact_id:[38422] //user id oa, channel, bot
        }).then(()=>{
            // console.log(admres.data.meta.already_been_in_contacts.length > 0)
            axios.post(`${process.env.apiUrl}`,{
                access_token:`${process.env.access_token}`,
                target_user_id:res.data.data[0].id
            }).then((getRoomId)=>{
                console.log(getRoomId.data.data.qiscus_room_id)
                let roomIdPo = getRoomId.data.data.qiscus_room_id
                // console.log(reqData[room_id])
                pesan = 'Haloo mas sony! Vira mau infoin nih kalo ada request dari tim '+reqData[room_id].team.toUpperCase()+ ' Berikut rincian nya:\n\n'+
                        'Request dari tim: '+ reqData[room_id].team.toUpperCase()+'\n'+
                        'PIC: '+reqData[room_id].from+'\n'+
                        'Request:\n\n Request nya dapat dilihat di: \n'+
                        reqData[room_id].req+'\n\n'+
                        'Deadline: Seminggu dari sekarang'
                send.Txt(pesan,roomIdPo)
            })
        })
    })
}
// sendtoPO()
server.get('/done', (req, res) => {
    var comment = ''
    var room_id = 5649116
    jira.board.getAllSprints({
        boardId: 931
    }, (error, data) => {
        data.values.filter((x) => {
            if (x.state === 'active') {
                jira.sprint.getSprintIssues({
                    sprintId: x.id
                }, (err, data) => {
                    data.issues.filter((y) => {
                        if (y.fields.status.name === 'Done') {
                            var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                            comment = comment + "\n" +
                                y.key + "\n" +
                                "Backlog: " + y.fields.summary + "\n" +
                                "PIC: " + pic + "\n" +
                                "Status: " + y.fields.status.name + "\n"
                        }
                    })
                    comment = "🚨 Backlog Done di " + x.name + " 🚨\n\n" + comment
                    send.Txt(comment, room_id)
                })
            }
        })
    })
})
server.get('/todo', (req, res) => {
    var comment = ''
    var room_id = 5649116
    jira.board.getAllSprints({
        boardId: 931
    }, (error, data) => {
        data.values.filter((x) => {
            if (x.state === 'active') {
                jira.sprint.getSprintIssues({
                    sprintId: x.id
                }, (err, data) => {
                    data.issues.filter((y) => {
                        if (y.fields.status.name === 'TO DO') {
                            var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                            comment = comment + "\n" +
                                y.key + "\n" +
                                "Backlog: " + y.fields.summary + "\n" +
                                "PIC: " + pic + "\n" +
                                "Status: " + y.fields.status.name + "\n"
                        }
                    })
                    comment = "🚨 Backlog To Do di " + x.name + " 🚨\n\n" + comment
                    send.Txt(comment, room_id)
                })
            }
        })
    })
})
server.post('/', (req, res) => {
    // Bot terima input dari user berupa room_id, pesan, dan nama user
    room_id = res.req.body.chat_room.qiscus_room_id
    let msg = res.req.body.message.text
    nama = res.req.body.from.fullname
    //konversi pesan yang mengandung huruf kapital menjadi huruf kecil
    let lower = msg.toLowerCase()
    let getMsg = res.req.body.message
    // state[room_id] = lower

    //Condition for handling state
    if (state[room_id] === '/vira' || state[room_id] === 'kembali' || state[room_id] === 'batal' || state[room_id] === undefined) {
        state[room_id] = getMsg.text.toLowerCase()
    } else if (state[room_id] === 'request') {
        state[room_id] = getMsg.text.toLowerCase()
    } else if (state[room_id] === 'summary') {
        if (getMsg.text.toLowerCase() === 'tidak') {
            (async () => {
                pesan = 'Oke kak, terima kasih telah melakukan request! Untuk melakukan request kembali panggil vira dengan ketik: /vira yaa'
                await send.Txt(pesan, room_id)
            })()
        } else if(getMsg.text.toLowerCase() === 'ya') {
            //add code for send reqData to another room (mas sony)
            (async()=>{
                await sendtoPO()
                pesan = 'Request nya sudah vira teruskan ya kak. terima kasih telah melakukan request! Untuk melakukan request kembali panggil vira dengan ketik: /vira yaa'
                await send.Txt(pesan, room_id)
            })()
        }
    }
    //condition handling button click (postback from user)
    if (lower === '/vira' || lower === 'kembali' || lower === 'batal') {
        pesan = 'Halo ' + nama + ' ! Aku VIRA - Virtual Jira Assistant untuk Biz Team. Silakan pilih menu yang akan kamu pilih'
        payload = {
            'text': pesan,
            'buttons': [{
                    'label': 'Request',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'PIC',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'help',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                }
            ]
        }
        send.Btn(room_id, payload)
        state[room_id] = lower
    } else if (lower === 'request') {
        pesan = 'Halo kak ' + nama + '! FYI, karena ada limitasi teks yang dikirim, untuk melakukan request pastikan kakak kirim request nya dalam bentuk dokumen ya (gdrive, dkk.)'
        payload = {
            'text': pesan,
            'buttons': [{
                    'label': 'Growth',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Simply',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Batal',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },

            ]
        }
        send.Btn(room_id, payload)
        // reqData[room_id] = {'from' : nama, 'team':'','req':''}
    } else if (lower === 'pic') {
        let ram_list = ''
        let alim_list = ''
        for (var i = 0; i < list.length; i++) {
            if (list[i].pic === 'Rama') {
                ram_list = ram_list + "- " + list[i].name + " (" + list[i].cat + ")\n"
            } else {
                alim_list = alim_list + "- " + list[i].name + " (" + list[i].cat + ")\n"
            }
        }
        pesan = 'Berikut ini adalah list PIC Tech    Support Biz Team\n\n' +
            '1. Rama\n' +
            ram_list + '\n\n' +
            '2. Alim\n' +
            alim_list
        payload = {
            'text': pesan,
            'buttons': [{
                'label': 'Kembali',
                'type': 'postback',
                'payload': {
                    'url': '#',
                    'method': 'get',
                    'payload': 'null'
                }
            }]
        }
        send.Btn(room_id, payload)
        // send.Txt(pesan,room_id)
    } else if (lower === 'help') {
        pesan = 'Selamat datang di menu bantuan'
        send.Txt(pesan, room_id)
    } else if (lower === 'growth' || lower === 'simply') {
        team = lower
        reqData[room_id] = {
            'from': nama,
            'team': team
        }
        pesan = 'Oke, silakan masukkan request-an nya kak! Pastikan dalam bentuk link ya'
        send.Txt(pesan, room_id)
        state[room_id] = pesan
    } else if (state[room_id] === 'Oke, silakan masukkan request-an nya kak! Pastikan dalam bentuk link ya') {
        (async () => {
            if (msg.match(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&\'\(\)\*\+,;=.]+$/g) !== null) {
                reqData[room_id] = {
                    'from': nama,
                    'team': team,
                    'req': msg
                }
                let psn = ''
                pesan = 'Oke, request-an kakak ada di ' + msg
                psn = '[Ringkasan]\n' +
                    'Request ini akan Vira teruskan ke PO untuk kemudian dibuatkan backlog nya yaa\n\n' +
                    'Tim: ' + reqData[room_id].team + '\n' +
                    'PIC: ' + reqData[room_id].from + '\n' +
                    'Request: ' + reqData[room_id].req + '\n' +
                    'Estimasi Selesai: Seminggu dari sekarang\n\n' +
                    'Apakah kakak yakin informasi ini telah valid?'

                await send.Txt(pesan, room_id)
                payload = {
                    'text': psn,
                    'buttons': [{
                            'label': 'Ya',
                            'type': 'postback',
                            'payload': {
                                'url': '#',
                                'method': 'get',
                                'payload': 'null'
                            }
                        },
                        {
                            'label': 'Tidak',
                            'type': 'postback',
                            'payload': {
                                'url': '#',
                                'method': 'get',
                                'payload': 'null'
                            }
                        }
                    ]
                }
                send.Btn(room_id, payload)
                state[room_id] = 'summary'
            } else {
                pesan = 'Maaf kak, request yang kami terima hanya dalam bentuk link ya. Coba kirim lagi dalam bentuk link'
                send.Txt(pesan, room_id)
            }
        })()
    } else if(state[room_id] === 'summary'){
        if(lower === 'ya' || lower === 'tidak'){
           state[room_id] = 'kembali'
        }else{
            pesan = 'Maaf kak, kakak belum konfirmasi info diatas o:)'
            send.Txt(pesan, room_id)
        }
    }
    else {
        // if (state[room_id] !== 'summary') {
            (async () => {
                pesan = 'Mungkin kakak bisa coba klik menu dibawah ini'
                payload = {
                    'text': pesan,
                    'buttons': [{
                            'label': 'Request',
                            'type': 'postback',
                            'payload': {
                                'url': '#',
                                'method': 'get',
                                'payload': 'null'
                            }
                        },
                        {
                            'label': 'PIC',
                            'type': 'postback',
                            'payload': {
                                'url': '#',
                                'method': 'get',
                                'payload': 'null'
                            }
                        },
                        {
                            'label': 'help',
                            'type': 'postback',
                            'payload': {
                                'url': '#',
                                'method': 'get',
                                'payload': 'null'
                            }
                        }
                    ]
                }
                await send.Txt('Maaf kak, vira ga ngerti maksud kakak', room_id)
                send.Btn(room_id, payload)
            })()
        // }

        // state[room_id] = lower
    }
    console.log("state " + state[room_id])
    // console.log(reqData[room_id])
    //-----------------------------
    // JANGAN DIHAPUS DULU BIARIN AJA
    //-----------------------------
    //menu biz tech team dan biz squad
    if (lower === 'biz tech team') {
        pesan = 'Menu backlog Biz Tech Team'
        payload = {
            'text': pesan,
            'buttons': [{
                    'label': 'Backlog Berlangsung',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Tracking Developer',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                }
            ]
        }
        send.Btn(room_id, payload)
    } else if (lower === 'biz squad team') {
        pesan = 'Menu backlog Biz Squad Team'
        payload = {
            'text': pesan,
            'buttons': [{
                    'label': 'Backlog To Do',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Backlog In Progress',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Backlog Done',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Backlog Personal',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
            ]
        }
        send.Btn(room_id, payload)
    }

    //sub menu biz tech team level 1
    if (lower === 'backlog berlangsung') {
        pesan = 'Pilih status pengerjaan'
        payload = {
            'text': pesan,
            'buttons': [{
                    'label': 'To Do',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'In Development',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'In Merge',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'In QA',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
                {
                    'label': 'Done',
                    'type': 'postback',
                    'payload': {
                        'url': '#',
                        'method': 'get',
                        'payload': 'null'
                    }
                },
            ]
        }
        send.Btn(room_id, payload)
    }

    //sub menu biz tech team > status pengerjaan
    if (lower === 'to do') {
        var comment = ''
        jira.board.getAllSprints({
            boardId: 931
        }, (error, data) => {
            filterTask(data)
        })
    }

    if (lower === 'ram') {
        var todo = ''
        var progress = ''
        var qa = ''
        var reject = ''
        var done = ''
        jira.board.getAllSprints({
            boardId: 931
        }, (error, data) => {
            data.values.filter((x, i) => {
                if (x.state === 'active') {
                    jira.sprint.getSprintIssues({
                        sprintId: x.id
                    }, (err, data) => {
                        data.issues.filter((y) => {
                            if (y.fields.assignee !== null) {
                                if (y.fields.status.name === 'TO DO' && y.fields.assignee.displayName === 'Rama Aditya') {
                                    console.log(y.fields.assignee.displayName)
                                    todo = todo + "\n" +
                                        y.key + "\n" +
                                        "Backlog: " + y.fields.summary + "\n" +
                                        "PIC: " + y.fields.assignee.displayName + "\n" +
                                        "Status: " + y.fields.status.name + "\n"
                                } else if (y.fields.status.name === 'In Progress' && y.fields.assignee.displayName === 'Rama Aditya') {
                                    progress = progress + "\n" +
                                        y.key + "\n" +
                                        "Backlog: " + y.fields.summary + "\n" +
                                        "PIC: " + y.fields.assignee.displayName + "\n" +
                                        "Status: " + y.fields.status.name + "\n"
                                } else if (y.fields.status.name === 'In QA' && y.fields.assignee.displayName === 'Rama Aditya') {
                                    qa = qa + "\n" +
                                        y.key + "\n" +
                                        "Backlog: " + y.fields.summary + "\n" +
                                        "PIC: " + y.fields.assignee.displayName + "\n" +
                                        "Status: " + y.fields.status.name + "\n"
                                } else if (y.fields.status.name === 'In QA' && y.fields.assignee.displayName === 'Rama Aditya') {
                                    qa = qa + "\n" +
                                        y.key + "\n" +
                                        "Backlog: " + y.fields.summary + "\n" +
                                        "PIC: " + y.fields.assignee.displayName + "\n" +
                                        "Status: " + y.fields.status.name + "\n"
                                } else if (y.fields.status.name === 'Reject' && y.fields.assignee.displayName === 'Rama Aditya') {
                                    reject = reject + "\n" +
                                        y.key + "\n" +
                                        "Backlog: " + y.fields.summary + "\n" +
                                        "PIC: " + y.fields.assignee.displayName + "\n" +
                                        "Status: " + y.fields.status.name + "\n"
                                } else if (y.fields.status.name === 'Done' && y.fields.assignee.displayName === 'Rama Aditya') {
                                    done = done + "\n" +
                                        y.key + "\n" +
                                        "Backlog: " + y.fields.summary + "\n" +
                                        "PIC: " + y.fields.assignee.displayName + "\n" +
                                        "Status: " + y.fields.status.name + "\n"
                                }
                            }
                        })
                        todo = "🚨 [Biz Team Board]🚨\nTo Do di " + x.name + "\n\n" + todo
                        progress = "🚨 [Biz Team Board]🚨\nIn Progress di " + x.name + "\n\n" + progress
                        qa = "🚨 [Biz Team Board]🚨\nIn QA di " + x.name + "\n\n" + qa
                        reject = "🚨 [Biz Team Board]🚨\nReject di " + x.name + "\n\n" + reject
                        done = "🚨 [Biz Team Board]🚨\nDone di " + x.name + "\n\n" + done
                        send.Txt(todo, room_id)
                        send.Txt(progress, room_id)
                        send.Txt(qa, room_id)
                        send.Txt(reject, room_id)
                        send.Txt(done, room_id)
                    })
                }
            })
            // let len = data.values.length -1
            // console.log(data.values[len - 1].state)
            // if(data.values[len - 1].state === 'closed'){
            //     send.Txt('Sprint '+data.values[len - 1].name+' sudah ditutup', room_id)
            // }
            closed(data)
        })
    }

    if (lower.match(/done sprint ini/g)) {
        // var data = []
        var comment = ''
        jira.board.getAllSprints({
            boardId: 931
        }, (error, data) => {
            data.values.filter((x) => {
                if (x.state === 'active') {
                    jira.sprint.getSprintIssues({
                        sprintId: x.id
                    }, (err, data) => {
                        data.issues.filter((y) => {
                            if (y.fields.status.name === 'Done') {
                                var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                                comment = comment + "\n" +
                                    y.key + "\n" +
                                    "Backlog: " + y.fields.summary + "\n" +
                                    "PIC: " + pic + "\n" +
                                    "Status: " + y.fields.status.name + "\n"
                            }
                        })
                        comment = "🚨 Backlog Done di " + x.name + " 🚨\n\n" + comment
                        send.Txt(comment, room_id)
                    })
                }
            })
            closed(data)
        })
    } else if (lower.match(/in progress sprint ini/g)) {
        // var data = []
        var comment = ''
        jira.board.getAllSprints({
            boardId: 931
        }, (error, data) => {
            data.values.filter((x) => {
                if (x.state === 'active') {
                    jira.sprint.getSprintIssues({
                        sprintId: x.id
                    }, (err, data) => {
                        data.issues.filter((y) => {
                            if (y.fields.status.name === 'In Progress') {
                                var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                                comment = comment + "\n" +
                                    y.key + "\n" +
                                    "Backlog: " + y.fields.summary + "\n" +
                                    "PIC: " + pic + "\n" +
                                    "Status: " + y.fields.status.name + "\n"
                            }
                        })
                        comment = "🚨 Backlog In Progress di " + x.name + " 🚨\n\n" + comment
                        send.Txt(comment, room_id)
                    })
                }
            })
            closed(data)
        })
    } else if (lower.match(/in qa sprint ini/g)) {
        // var data = []
        var comment = ''
        jira.board.getAllSprints({
            boardId: 931
        }, (error, data) => {
            data.values.filter((x) => {
                if (x.state === 'active') {
                    jira.sprint.getSprintIssues({
                        sprintId: x.id
                    }, (err, data) => {
                        data.issues.filter((y) => {
                            if (y.fields.status.name === 'In QA') {
                                var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                                comment = comment + "\n" +
                                    y.key + "\n" +
                                    "Backlog: " + y.fields.summary + "\n" +
                                    "PIC: " + pic + "\n" +
                                    "Status: " + y.fields.status.name + "\n"
                            }
                        })
                        comment = "🚨 Backlog In QA di " + x.name + " 🚨\n\n" + comment
                        send.Txt(comment, room_id)
                    })
                }
            })
            closed(data)
        })
    } else if (lower.match(/to do sprint ini/g)) {
        // var data = []
        var comment = ''
        jira.board.getAllSprints({
            boardId: 931
        }, (error, data) => {
            data.values.filter((x) => {
                if (x.state === 'active') {
                    jira.sprint.getSprintIssues({
                        sprintId: x.id
                    }, (err, data) => {
                        data.issues.filter((y) => {
                            if (y.fields.status.name === 'TO DO') {
                                var pic = y.fields.assignee !== null ? y.fields.assignee.displayName : '- not assigned-'
                                comment = comment + "\n" +
                                    y.key + "\n" +
                                    "Backlog: " + y.fields.summary + "\n" +
                                    "PIC: " + pic + "\n" +
                                    "Status: " + y.fields.status.name + "\n"
                            }
                        })
                        comment = "🚨 Backlog To Do di " + x.name + " 🚨\n\n" + comment
                        send.Txt(comment, room_id)
                    })
                }
            })
            closed(data)
        })
    }
})

server.listen((process.env.PORT || 3000), () => {
    console.log("Server is up and running..." + `${process.env.PORT}`);
});