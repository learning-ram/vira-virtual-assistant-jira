// require('dotenv').config()
if(process.env.NODE_ENV === 'production'){
    require('custom-env').env('production')
}
else{
    require('custom-env').env('development')
}

const token = `${process.env.access_token}`
const apiUrl = `${process.env.apiUrl}`
const axios = require('axios');

module.exports = {
    Txt: async(msg, room_id) => {
        await axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'text',
            comment: msg
        })
        .then(()=>{}).catch((err)=>{console.log(err.response.data)})
    },
    Btn: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'buttons',
            payload: JSON.stringify(payload)
        });
    },
    Carousel: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'carousel',
            payload: JSON.stringify(payload)
        });
    },
    Card: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'card',
            payload: JSON.stringify(payload)
        })
    },
    File:(room_id,payload)=>{
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'file_attachment',
            payload: JSON.stringify(payload)
        })
    }

}